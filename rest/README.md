# Example: rest

This example shows the minimum requirements to:
* set up an osgi bundle with rest api

## Rest-Api

Documentation can be found here:
https://docs.elo.com/dev/programming/de-de/plug-ins/indexserver-plug-in-development/rest-plug-in.html#openapi-schnittstellenbeschreibung-im-json-format


## Depedencies

2 new dependencies are used for the rest-plugin

* openapi-generator
* rest-plugin

In this example we include the dependencies in a seperate folder called lib.
You can find these dependencies under this path (path may differ for your installation)
``
D:\ELOprofessional\servers\ELO-1\webapps\ix-Archiv\resources\ix-osgi-platform\compile
``


Copy these 2 dependencies in the lib folder and include them.
