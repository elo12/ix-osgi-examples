package org.example.osgi.basic;

import de.elo.ix.client.plugin.PluginActivator;
import org.osgi.framework.BundleContext;

import de.elo.ix.plugin.rest.api.RestApplication;

public class Activator extends PluginActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);

        // Build a RestApplication object. Add the application and
        // resource classes.
        RestApplication application = RestApplication.builder()
                .add(Application.class)
                .add(Hello.class)
                .build();

        // Register the RestApplication object.
        registerService(RestApplication.class, application);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        unregisterService(RestApplication.class);
        super.stop(context);
    }
}
