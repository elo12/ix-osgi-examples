package org.example.osgi.basic;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * This resource shows how to implement a REST API.
 */
@Path("myrest")
public class Hello {

    /**
     * Add two numbers.
     * This function adds two floating point numbers and returns the result.
     *
     * @param op1 First number
     * @param op2 Second number
     * @return Sum of given numbers
     */
    @GET
    @Path("add")
    @Produces(MediaType.APPLICATION_JSON)
    public double add(@QueryParam("op1") double op1, @QueryParam("op2") double op2) {
        return op1 + op2;
    }
}