package org.example.osgi.basic;

import jakarta.ws.rs.ApplicationPath;


/**
 * My first REST service.
 *
 * This service provides functions to access an ELO repository.
 *
 * @version 1.0.0
 */
@ApplicationPath("myrest")
public class Application {
}