package org.example.osgi.event;

import de.elo.ix.client.IXServerEvents;
import de.elo.ix.client.plugin.PluginActivator;
import org.osgi.framework.BundleContext;

public class Activator extends PluginActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);

        registerService(IXServerEvents.class, new IXServerEventsBaseImpl(this));
    }
}
