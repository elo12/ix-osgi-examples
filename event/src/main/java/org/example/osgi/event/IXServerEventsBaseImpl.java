package org.example.osgi.event;

import byps.RemoteException;
import de.elo.ix.client.Any;
import de.elo.ix.client.IXServerEventsContext;
import de.elo.ix.client.LoginScriptOptions;
import de.elo.ix.client.plugin.IXServerEventsBase;
import de.elo.ix.client.plugin.PluginActivator;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IXServerEventsBaseImpl extends IXServerEventsBase {

    public IXServerEventsBaseImpl(PluginActivator activator) {
        super(activator);
    }

    /**
     * This method is called asynchronously after a user has been successfully authenticated.
     *
     * @param ec       Execution context
     * @param userName of the user that has been logged in successfully
     * @param opts     additional information
     * @throws RemoteException
     */
    @Override
    public void onAfterLogin(IXServerEventsContext ec, String userName, LoginScriptOptions opts) throws RemoteException {
        log.info("User {} logged in successful", userName);
    }

    /**
     * This functions returns the input
     *
     * @param ec
     * @param args
     * @return
     * @throws RemoteException
     */
    public String RF_StringEchoFunction(IXServerEventsContext ec, String args) throws RemoteException {
        return args;
    }

    /**
     * This functions returns the input
     *
     * @param ec
     * @param args
     * @return
     * @throws RemoteException
     */
    public Any RF_AnyEchoFunction(IXServerEventsContext ec, Any args) throws RemoteException {
        String arguments = args.getStringValue();

        return new Any(arguments);
    }
}
