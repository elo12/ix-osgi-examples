# Example: event

This example shows the minimum requirements to:
* set up an osgi bundle
* add ix server events
* add remote functions

## Server Events:

Documentation can be found here:
* https://forum.elo.com/javadoc/ix/23/de/elo/ix/client/IXServerEvents.html


## Remote Functions

There are 2 types of RF. String and Any functions.

String functions get called client side with
``
String executeRegisteredFunctionString(String functionName, String param)
``

Any functions get called client side with
``
Any executeRegisteredFunction(String functionName, Any param)
``
