# Example: workflow

This example shows the minimum requirements to:
* call an osgi bundle from a workflow node

## Workflows:

Documentation can be found here:
* https://docs.elo.com/dev/programming/de-de/plug-ins/indexserver-plug-in-development/plug-in-services.html#plug-in-service-workflownodeeventsfactory


### WorkflowNodeEventsFactoryImpl

Factory class to select the correct WorkflowNode implementation based on the script-path

For this package in the workflowdesigner under end-script you would enter:

``org.example.osgi.workflow/wf-incoming``

### WorkflowNodeEventsImpl

Similar to a JavaScript with the functions onEnterWorkflowNode and onExitWorkflowNode
