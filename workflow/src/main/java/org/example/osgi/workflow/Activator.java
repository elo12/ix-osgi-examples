package org.example.osgi.workflow;

import de.elo.ix.client.IXServerEvents;
import de.elo.ix.client.plugin.PluginActivator;
import de.elo.ix.client.plugin.WorkflowNodeEventsFactory;
import org.osgi.framework.BundleContext;

public class Activator extends PluginActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);

        registerService(WorkflowNodeEventsFactory.class, new WorkflowNodeEventsFactoryImpl(this));
    }
}
