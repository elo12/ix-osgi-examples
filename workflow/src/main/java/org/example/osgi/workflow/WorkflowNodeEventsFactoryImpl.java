package org.example.osgi.workflow;

import byps.RemoteException;
import de.elo.ix.client.IXServerEventsContext;
import de.elo.ix.client.plugin.PluginActivator;
import de.elo.ix.client.plugin.WorkflowNodeEvents;
import de.elo.ix.client.plugin.WorkflowNodeEventsFactory;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class WorkflowNodeEventsFactoryImpl implements WorkflowNodeEventsFactory {

    private final PluginActivator activator;

    @Override
    public WorkflowNodeEvents create(IXServerEventsContext ixServerEventsContext, String path) throws RemoteException {
        WorkflowNodeEvents impl = null;
        if (path.equals("/wf-incoming")) {
            impl = new WorkflowNodeEventsImpl(activator);
        }
        return impl;
    }
}
