package org.example.osgi.workflow;

import byps.RemoteException;
import de.elo.ix.client.IXServerEventsContext;
import de.elo.ix.client.WFDiagram;
import de.elo.ix.client.plugin.PluginActivator;
import de.elo.ix.client.plugin.WorkflowNodeEvents;

public class WorkflowNodeEventsImpl implements WorkflowNodeEvents {

    public WorkflowNodeEventsImpl(PluginActivator activator) {
    }

    @Override
    public Object onEnterWorkflowNode(IXServerEventsContext ixServerEventsContext, WFDiagram wfDiagram, int i) throws RemoteException {
        return null;
    }

    @Override
    public Object onExitWorkflowNode(IXServerEventsContext ixServerEventsContext, WFDiagram wfDiagram, int i) throws RemoteException {
        return null;
    }
}
